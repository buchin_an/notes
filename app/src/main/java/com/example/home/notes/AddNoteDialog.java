package com.example.home.notes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.EditText;

import com.example.home.notes.model.Note;

public class AddNoteDialog extends AppCompatDialogFragment {
    EditText addNoteTitle;
    EditText addNoteText;
    private NoteCreator noteCreator;

    public interface NoteCreator {
        void createNote(Note note);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_note, null);
        addNoteTitle = view.findViewById(R.id.dialog_add_title);
        addNoteText = view.findViewById(R.id.dialog_add_text);
        builder.setView(view)
                .setTitle(R.string.add_note_title)
                .setPositiveButton(R.string.add_note_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        noteCreator.createNote(new Note(R.mipmap.ic_launcher, addNoteTitle.getText().toString()
                                , addNoteText.getText().toString()));

                    }
                })
                .setNegativeButton(R.string.add_note_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    public void setNoteCreator(NoteCreator noteCreator) {
        this.noteCreator = noteCreator;
    }
}
