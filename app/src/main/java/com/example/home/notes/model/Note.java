package com.example.home.notes.model;

public class Note {
    private int icon;
    private String title;
    private String text;
    private String textPreview;

    public Note(int icon, String title, String text) {
        this.icon = icon;
        this.title = title;
        this.text = text;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getTextPreview() {
        return text.substring(0,14);
    }
}
