package com.example.home.notes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.home.notes.adapter.NotesAdapter;
import com.example.home.notes.model.Note;

public class ListFragment extends Fragment {

    RecyclerView notes;
    NotesAdapter adapter;
    NotesList notesList;




    public static ListFragment getInstance() {
        return new ListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        notesList = new NotesList();
        notes = root.findViewById(R.id.notes_list);
        adapter = new NotesAdapter(notesList.getAllNotes(),getContext(),this);
        adapter.setNoteListener(NoteFragment.getInstance());
        notes.setLayoutManager(new LinearLayoutManager(getContext()));
        notes.setAdapter(adapter);

        return root;
    }
}
