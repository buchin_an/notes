package com.example.home.notes;

import com.example.home.notes.model.Note;

import java.util.ArrayList;
import java.util.List;

public class DefaultNotes {
    public static ArrayList<Note> getDefaultNotes() {
        ArrayList<Note> notes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            notes.add(new Note(R.mipmap.ic_launcher, "Lorem ipsum dolor sit amet "+i,
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, " +
                            "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea " +
                            "commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit " +
                            "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat " +
                            "cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
        }
        return notes;
    }
}
