package com.example.home.notes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.home.notes.adapter.NotesAdapter;
import com.example.home.notes.model.Note;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteFragment extends Fragment implements NotesAdapter.NoteInteractListener {
    private Note note;
    @BindView(R.id.note_title)
    TextView noteTitle;
    @BindView(R.id.note_text)
    TextView noteText;

    private static NoteFragment instance;

    public static NoteFragment getInstance() {
        if (instance == null) {
            instance = new NoteFragment();
        }

        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_note, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void show(Note note) {

        noteTitle.setText(note.getTitle());
        noteText.setText(note.getText());


    }
}
