package com.example.home.notes;

import com.example.home.notes.model.Note;

import java.util.ArrayList;
import java.util.List;

public class NotesList {

    private ArrayList<Note> notes = DefaultNotes.getDefaultNotes();

    public void addNote(Note note) {
        notes.add(note);
    }

    public void deleteNote(int position) {
        notes.remove(position);
    }

    public List<Note> getAllNotes() {
        return notes;
    }
}
