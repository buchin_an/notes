package com.example.home.notes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListFragment listFragment = ListFragment.getInstance();
        NoteFragment noteFragment = NoteFragment.getInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_list, listFragment)
                .commit();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_note, noteFragment)
                .commit();

    }
}
