package com.example.home.notes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.home.notes.AddNoteDialog;
import com.example.home.notes.R;
import com.example.home.notes.model.Note;

import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> implements AddNoteDialog.NoteCreator {

    private List<Note> notes;
    private NoteInteractListener noteListener;
    Context context;
    Fragment fragment;

    @Override
    public void createNote(Note note) {
        notes.add(note);
        notifyDataSetChanged();
    }

    public interface NoteInteractListener {
        void show(Note note);
    }

    public NotesAdapter(List<Note> notes, Context context, Fragment fragment) {
        this.notes = notes;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.item_note, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.icon.setImageResource(notes.get(position).getIcon());
        holder.text.setText(notes.get(position).getTextPreview());
        holder.title.setText(notes.get(position).getTitle());

    }

    public Note getItem(int position) {
        return notes.get(position);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView text;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.note_title);
            text = itemView.findViewById(R.id.note_text);
            icon = itemView.findViewById(R.id.note_icon);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(context, v);
                    popupMenu.inflate(R.menu.menu_note);

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_add:
                                    AddNoteDialog addNoteDialog = new AddNoteDialog();
                                    addNoteDialog.setNoteCreator(NotesAdapter.this);
                                    addNoteDialog.show(fragment.getFragmentManager(), "tag");
                                    notifyDataSetChanged();
                                    return true;
                                case R.id.menu_delete:
                                    removeNote(getAdapterPosition());
                                    notifyItemRangeRemoved(getAdapterPosition(), 1);

                                default:
                                    return false;
                            }
                        }
                    });
                    popupMenu.show();
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noteListener.show(getItem(getAdapterPosition()));
                }
            });
        }

    }

    public void setNoteListener(NoteInteractListener noteListener) {
        this.noteListener = noteListener;
    }

    private void removeNote(int position) {
        notes.remove(position);
    }
}
